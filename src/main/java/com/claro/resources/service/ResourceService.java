package com.claro.resources.service;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.claro.resources.bean.resources.CanonicalResponseMessage;
import com.claro.resources.bean.resources.CanonicalDeleteResponseMessage;
import com.claro.resources.bean.resources.ObjectCanonicalResponseMessage;
import com.claro.resources.bean.resources.ObjectCanonicalDeleteResponseMessage;
import com.claro.resources.bean.resources.RequestResource;
import com.claro.resources.util.JsonUtil;
import com.claro.resources.util.TracingHelper;

import io.opentracing.Span;
import io.opentracing.Tracer;

@Service
public class ResourceService<T> {

	private static final Logger logger = LogManager.getLogger(ResourceService.class.getName());

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private JsonUtil jsonUtil;

	@Autowired
	Tracer tracer;

	/**
	 * 
	 * @param requestResource
	 * @param uriMGWServletRest
	 * @param authorization
	 * @param idRequestMGWGetResource
	 * @param uriQueryAddress
	 * @param codeCountry
	 * @return ResourceResponseMessage
	 * @throws IOException
	 */
	public CanonicalResponseMessage invokeGetRestMgwResource(String uriMGWServletRest, String authorization,
			String idRequestMGWGetResource, Integer transactionId, String resourceId)
			throws IOException {

		CanonicalResponseMessage response = null;
		ObjectCanonicalResponseMessage responseRest = null;

		HttpEntity<Object> request = null;
		HttpHeaders headers = new HttpHeaders();
		
		Span span;
		if (transactionId != null)
			span = tracer.buildSpan("invoke").start().setTag("transaction.id", transactionId);
		else
			span = tracer.buildSpan("invoke").start();

		try {

			headers.set("Authorization", authorization);
			headers.set("idRequest", idRequestMGWGetResource);
			headers.set("resourceId", resourceId);
			headers.setContentType(MediaType.APPLICATION_JSON);

			if (resourceId != null) {

				// Preparar request para crear recurso en OFSC a través de Microgateway
				request = new HttpEntity<Object>(headers);

				responseRest = restTemplate.postForObject(uriMGWServletRest, request,
						ObjectCanonicalResponseMessage.class);
				response = responseRest.getResourceResponseMessage();

				if (response != null) 
					logger.info("responseGetResourceOFSC => " + response);
				else
					logger.info("responseGetResourceOFSC es nulo ");
				
				TracingHelper.onSuccess(span, 200);
			}

		} catch(HttpClientErrorException e) {
			logger.error("MSA-Resource Client(1-GetResourceMGW): " + e.getMessage(), e);
			logger.error("MSA-Resource Client(2-GetResourceMGW):  Http Status Code: " + e.getStatusCode()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));

			response = new CanonicalResponseMessage();
			response.setCode("999");
			response.setCodeCatch(String.valueOf(e.getStatusCode().value()));
			response.setMessage("Message: " + e.getMessage()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));
			
			TracingHelper.onError(e, span);
			
		} catch(HttpServerErrorException e) {
			logger.error("MSA-Resource Server(1-GetResourceMGW): " + e.getMessage(), e);
			logger.error("MSA-Resource Server(2-GetResourceMGW): Http Status Code: " + e.getStatusCode()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));

			response = new CanonicalResponseMessage();
			response.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			response.setCodeCatch(String.valueOf(e.getStatusCode().value()));
			response.setMessage("Message: " + e.getMessage()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));
			
			TracingHelper.onError(e, span);
			
		} catch (Exception e){
			logger.error("MSA-Resource Exception(GetResourceInvokeMGW): " + e.getMessage());
			
			response = new CanonicalResponseMessage();
			response.setCode("998");
			response.setCodeCatch(String.valueOf(HttpStatus.BAD_REQUEST.value()));
			response.setMessage("Message: " + e.getMessage());		
			
			TracingHelper.onError(e, span);
		}
		
		span.finish();
		return response;
	}
	
	/**
	 * 
	 * @param requestResource
	 * @param uriMGWServletRest
	 * @param authorization
	 * @param idRequestMGWCreateResource
	 * @param uriQueryAddress
	 * @param codeCountry
	 * @return
	 * @throws IOException
	 */
	public CanonicalResponseMessage invokeCreateRestMgwResource(RequestResource requestResource,
			String uriMGWServletRest, String authorization, String idRequestMGWCreateResource, 
			Integer transactionId, String resourceId) throws IOException {

		CanonicalResponseMessage response = null;
		ObjectCanonicalResponseMessage responseRest = null;

		HttpEntity<Object> request = null;
		HttpHeaders headers = new HttpHeaders();
		String jsonFinal = "";
		
		Span span;
		if (transactionId != null)
			span = tracer.buildSpan("invoke").start().setTag("transaction.id", transactionId);
		else
			span = tracer.buildSpan("invoke").start();

		try {

			headers.set("Authorization", authorization);
			headers.set("idRequest", idRequestMGWCreateResource);
			headers.set("resourceId", resourceId);
			headers.setContentType(MediaType.APPLICATION_JSON);

			if (requestResource != null) {

				// Convertir requestResource to Json
				jsonFinal = jsonUtil.convertirAJson(requestResource);

				logger.info("jsonRequestResource " + jsonFinal);

				// Preparar request para crear recurso en OFSC a través de Microgateway
				request = new HttpEntity<Object>(jsonFinal, headers);

				responseRest = restTemplate.postForObject(uriMGWServletRest, request,
						ObjectCanonicalResponseMessage.class);
				response = responseRest.getResourceResponseMessage();

				if(response!=null) {
					if(response.getResponse()!=null) {
						if(response.getResponse().getResource() !=null)
							logger.info("Recurso creado en OFSC # " + response.getResponse().getResource().getResourceId());
					} else
						logger.error("getReponse de JSON CreateResourceOFSC es nulo");
					
					
					logger.debug("JSON responseCreateResourceOFSC => " + response.toString());
				}else
					logger.info("responseCreateResourceOFSC es nulo ");
				
				TracingHelper.onSuccess(span, 201);
			}

		} catch(HttpClientErrorException e) {
			logger.error("MSA-Resource Client(1-CreateResourceMGW): " + e.getMessage());
			logger.error("MSA-Resource Client(2-CreateResourceMGW):  Http Status Code: " + e.getStatusCode()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));

			response = new CanonicalResponseMessage();
			response.setCode("999");
			response.setCodeCatch(String.valueOf(e.getStatusCode().value()));
			response.setMessage("Message: " + e.getMessage()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));
			
			TracingHelper.onError(e, span);
			
		} catch(HttpServerErrorException e) {
			logger.error("MSA-Resource Server(1-CreateResourceMGW): " + e.getMessage());
			logger.error("MSA-Resource Server(2-CreateResourceMGW): Http Status Code: " + e.getStatusCode()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));

			response = new CanonicalResponseMessage();
			response.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			response.setCodeCatch(String.valueOf(e.getStatusCode().value()));
			response.setMessage("Message: " + e.getMessage()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));
			
			TracingHelper.onError(e, span);
			
		} catch (Exception e){
			logger.error("MSA-Resource Exception(CreateResourceInvokeMGW): " + e.getMessage());
			
			response = new CanonicalResponseMessage();
			response.setCode("998");
			response.setCodeCatch(String.valueOf(HttpStatus.BAD_REQUEST.value()));
			response.setMessage("Message: " + e.getMessage());
			
			TracingHelper.onError(e, span);
			
		}
		
		span.finish();
		return response;
	}

	/**
	 * 
	 * @param requestResource
	 * @param uriMGWServletRest
	 * @param authorization
	 * @param idRequestMGWUpdateResource
	 * @param uriQueryAddress
	 * @param codeCountry
	 * @return
	 * @throws IOException
	 */
	public CanonicalResponseMessage invokeUpdateRestMgwResource(RequestResource requestResource,
			String uriMGWServletRest, String authorization, String idRequestMGWUpdateResource,
			Integer transactionId, String resourceId) throws IOException {

		CanonicalResponseMessage response = null;
		ObjectCanonicalResponseMessage responseRest = null;

		HttpEntity<Object> request = null;
		HttpHeaders headers = new HttpHeaders();
		String jsonFinal = "";
		
		Span span;
		if (transactionId != null)
			span = tracer.buildSpan("invoke").start().setTag("transaction.id", transactionId);
		else
			span = tracer.buildSpan("invoke").start();

		try {

			headers.set("Authorization", authorization);
			headers.set("idRequest", idRequestMGWUpdateResource);
			headers.set("resourceId", resourceId);
			headers.setContentType(MediaType.APPLICATION_JSON);

			if (requestResource != null) {
				// Convertir requestResource to Json
				jsonFinal = jsonUtil.convertirAJson(requestResource);

				logger.info("jsonRequestResource " + jsonFinal);

				// Preparar request para actualizar recurso en OFSC a través de Microgateway
				request = new HttpEntity<Object>(jsonFinal, headers);

				responseRest = restTemplate.postForObject(uriMGWServletRest, request,
						ObjectCanonicalResponseMessage.class);

				response = responseRest.getResourceResponseMessage();

				if (response != null) 
					logger.info("responseUpdateResourceOFSC => " + response);
				 else
					logger.info("responseUpdateResourceOFSC es nulo ");
				
				TracingHelper.onSuccess(span, 200);
			}

		} catch(HttpClientErrorException e) {
			logger.error("MSA-Resource Client(1-UpdateResourceMGW): " + e.getMessage());
			logger.error("MSA-Resource Client(2-UpdateResourceMGW):  Http Status Code: " + e.getStatusCode()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));

			response = new CanonicalResponseMessage();
			response.setCode("999");
			response.setCodeCatch(String.valueOf(e.getStatusCode().value()));
			response.setMessage("Message: " + e.getMessage()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));
			
			TracingHelper.onError(e, span);
			
		} catch(HttpServerErrorException e) {
			logger.error("MSA-Resource Server(1-UpdateResourceMGW): " + e.getMessage());
			logger.error("MSA-Resource Server(2-UpdateResourceMGW): Http Status Code: " + e.getStatusCode()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));

			response = new CanonicalResponseMessage();
			response.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			response.setCodeCatch(String.valueOf(e.getStatusCode().value()));
			response.setMessage("Message: " + e.getMessage()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));
			
			TracingHelper.onError(e, span);
			
		} catch (Exception e){
			logger.error("MSA-Resource Exception(CreateResourceInvokeMGW): " + e.getMessage());
			
			response = new CanonicalResponseMessage();
			response.setCode("998");
			response.setCodeCatch(String.valueOf(HttpStatus.BAD_REQUEST.value()));
			response.setMessage("Message: " + e.getMessage());
			
			TracingHelper.onError(e, span);
			
		}
		
		span.finish();
		return response;
	}

	/**
	 * 
	 * @param requestResource
	 * @param uriMGWServletRest
	 * @param authorization
	 * @param idRequestMGWDeleteResource
	 * @param uriQueryAddress
	 * @param codeCountry
	 * @return
	 * @throws IOException
	 */
	public CanonicalDeleteResponseMessage invokeDeleteRestMgwResource(String uriMGWServletRest, String authorization,
			String idRequestMGWDeleteResource, Integer transactionId, String resourceId)
			throws IOException {

		CanonicalDeleteResponseMessage response = null;
		ObjectCanonicalDeleteResponseMessage responseRest = null;

		HttpEntity<Object> request = null;
		HttpHeaders headers = new HttpHeaders();
		
		Span span;
		if (transactionId != null)
			span = tracer.buildSpan("invoke").start().setTag("transaction.id", transactionId);
		else
			span = tracer.buildSpan("invoke").start();

		try {

			headers.set("Authorization", authorization);
			headers.set("idRequest", idRequestMGWDeleteResource);
			headers.set("resourceId", resourceId);
			headers.setContentType(MediaType.APPLICATION_JSON);

			if (resourceId != null) {
				// Preparar request para actualizar recurso en OFSC a través de Microgateway
				request = new HttpEntity<Object>(headers);

				responseRest = restTemplate.postForObject(uriMGWServletRest, request,
						ObjectCanonicalDeleteResponseMessage.class);

				response = responseRest.getDeleteResourceResponseMessage();

				if (response != null) 
					logger.info("responseDeleteResourceOFSC => " + response);
				 else
					logger.info("responseDeleteResourceOFSC es nulo ");
				
				TracingHelper.onSuccess(span, 200);
			}

		} catch(HttpClientErrorException e) {
			logger.error("MSA-Resource Client(1-DeleteResourceMGW): " + e.getMessage());
			logger.error("MSA-Resource Client(2-DeleteResourceMGW):  Http Status Code: " + e.getStatusCode()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));

			response = new CanonicalDeleteResponseMessage();
			response.setCode("999");
			response.setCodeCatch(String.valueOf(e.getStatusCode().value()));
			response.setMessage("Message: " + e.getMessage()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));
			
			TracingHelper.onError(e, span);
			
		} catch(HttpServerErrorException e) {
			logger.error("MSA-Resource Server(1-DeleteResourceMGW): " + e.getMessage());
			logger.error("MSA-Resource Server(2-DeleteResourceMGW): Http Status Code: " + e.getStatusCode()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));

			response = new CanonicalDeleteResponseMessage();
			response.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			response.setCodeCatch(String.valueOf(e.getStatusCode().value()));
			response.setMessage("Message: " + e.getMessage()
					+ (e.getResponseBodyAsString().length() > 0 ? ", responseBody: " + e.getResponseBodyAsString()
							: ""));
			
			TracingHelper.onError(e, span);
			
		} catch (Exception e){
			logger.error("MSA-Resource Exception(DeleteResourceMGW): " + e.getMessage());
			
			response = new CanonicalDeleteResponseMessage();
			response.setCode("998");
			response.setCodeCatch(String.valueOf(HttpStatus.BAD_REQUEST.value()));
			response.setMessage("Message: " + e.getMessage());
			
			TracingHelper.onError(e, span);
			
		}
		
		span.finish();
		return response;
	}
}
