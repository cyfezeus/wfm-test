package com.claro.resources.util;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * 
 * 
 * @author Gizlo
 *
 */
@Component
public class JsonUtil {
	

	@Autowired
	ObjectMapper objectMapper;
	
	/**
	 * 
	 * @param object Objeto Java a convertir
	 * @return
	 * @throws JsonProcessingException
	 */
	public String convertirAJson(Object object) throws JsonProcessingException {		
		return objectMapper.writeValueAsString(object);
	}
	
	/**
	 * 
	 * @param json String a convertir
	 * @param clase Clase resultante
	 * @throws IOException
	 */
	public <T> T convertirAObjeto(final String json, final Class<T> clase) 
			throws IOException {
		return objectMapper.readValue(json, clase);
	}
	
	/**
	 * 
	 * @param json String a convertir
	 * @param ref Referencia de tipos para objetos genericos
	 * @return Object, castear al tipo enviado.
	 * @throws IOException
	 */
	
	@SuppressWarnings("rawtypes")
	public Object convertirAObjeto(final String json, TypeReference ref) 
			throws IOException {				
		return objectMapper.readValue(json, ref);
	}
	
	
}
