package com.claro.resources.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import io.opentracing.Span;
import io.opentracing.tag.Tags;

public class TracingHelper {

	public static void onSuccess(Span span, Integer httpStatus) {
		Tags.HTTP_STATUS.set(span, httpStatus);
		span.setTag("response.code", 0);
		span.setTag("response.status", "OK");
		span.setTag("response.message", "Successful Response");
	}

	public static void onError(Throwable throwable, Span span) {
		Tags.HTTP_STATUS.set(span, 500);
		Tags.ERROR.set(span, Boolean.TRUE);
		if (throwable != null)
			span.log(errorLogs(throwable));

	}

	private static Map<String, Object> errorLogs(Throwable throwable) {
		Map<String, Object> errorLogs = new HashMap<>(4);
		errorLogs.put("event", Tags.ERROR.getKey());
		errorLogs.put("error.kind", throwable.getClass().getName());
		errorLogs.put("error.object", throwable);
		errorLogs.put("message", throwable.getMessage());

		StringWriter sw = new StringWriter();
		throwable.printStackTrace(new PrintWriter(sw));
		errorLogs.put("stack", sw.toString());

		return errorLogs;
	}

}
