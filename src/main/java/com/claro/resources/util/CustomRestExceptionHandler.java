package com.claro.resources.util;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * Intercepta los errores por body request
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		// logger.info(ex.getClass().getName());
		//
		final List<String> listError = new ArrayList<String>();
		for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
			listError.add(error.getField() + ": " + error.getDefaultMessage());
		}

		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			listError.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}

		ApiValidationError apiError = new ApiValidationError(HttpStatus.BAD_REQUEST.toString(), listError.toString());
		return new ResponseEntity<Object>(apiError, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Intercepta los errores por body request list
	 */
	// https://github.com/oarcher/brest2016/blob/master/brest2016/src/brest2016/spring/controller/ControllerExceptionHandler.java
	@ExceptionHandler(ConstraintViolationException.class)
	protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
		List<String> listError = new ArrayList<String>();
		for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			listError.add("Reference: " + violation.getPropertyPath() + ", message: " + violation.getMessage());
			logger.info(listError);
		}
		ApiValidationError apiError = new ApiValidationError(HttpStatus.BAD_REQUEST.toString(), listError.toString());
		return new ResponseEntity<Object>(apiError, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Intercepta los errores por headers request
	 */
	@Override
	protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		ApiValidationError apiError = new ApiValidationError(HttpStatus.BAD_REQUEST.toString(), ex.getMessage());
		return new ResponseEntity<Object>(apiError, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Intercepta los errores por enums request
	 */
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		ApiValidationError apiError = new ApiValidationError(HttpStatus.BAD_REQUEST.toString(), e.getMostSpecificCause().toString());
		return new ResponseEntity<Object>(apiError, HttpStatus.BAD_REQUEST);
	}

}