package com.claro.resources.util;

public class ApiValidationError {

	private String code;
	private String message;

	public ApiValidationError() {
		super();
	}

	public ApiValidationError(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
