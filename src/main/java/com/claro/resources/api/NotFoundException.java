package com.claro.resources.api;

@javax.annotation.Generated(value = "com.claro.resources.codegen.languages.SpringCodegen", date = "2019-02-12T14:27:01.801Z")

public class NotFoundException extends ApiException {
    private int code;
    public NotFoundException (int code, String msg) {
        super(code, msg);
        this.code = code;
    }
}
