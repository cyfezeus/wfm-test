package com.claro.resources.api;

@javax.annotation.Generated(value = "com.claro.resources.codegen.languages.SpringCodegen", date = "2019-02-12T14:27:01.801Z")

public class ApiException extends Exception{
    private int code;
    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }
}
