package com.claro.resources.api;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.claro.resources.bean.resources.CanonicalDeleteResponseMessage;
import com.claro.resources.bean.resources.CanonicalResponseMessage;
import com.claro.resources.bean.resources.RequestResource;
import com.claro.resources.bean.resources.ResourceDeleteResponseMessage;
import com.claro.resources.bean.resources.ResourceResponseMessage;
//import com.claro.resources.service.ResourceService;
import com.claro.resources.service.ResourceService;

import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "com.claro.resources.codegen.languages.SpringCodegen", date = "2019-02-12T14:27:01.801Z")

@Validated
@Controller
public class EnterpriseApiController implements EnterpriseApi {

	private static final Logger logger = LogManager.getLogger(EnterpriseApiController.class.getName());

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public EnterpriseApiController(HttpServletRequest request) {
		this.request = request;
	}

	@Autowired
	private ResourceService<?> resourceService;

	@Value("${MGW.uriServletRest}")
	private String uriMGWServletRest;

	@Value("${OFSC.authorization}")
	private String authorizationOFSC;

//	@Value("${OFSC.resourceId}")
//	private String resourceId;

	@Value("${MGW.idRequest.CreateResource}")
	private String idRequestMGWCreateResource;

	@Value("${MGW.idRequest.UpdateResource}")
	private String idRequestMGWUpdateResource;

	@Value("${MGW.idRequest.DeleteResource}")
	private String idRequestMGWDeleteResource;

	@Value("${MGW.idRequest.GetResource}")
	private String idRequestMGWGetResource;

	public ResponseEntity<?> getResource(
//			@ApiParam(value = "", required = true) @RequestHeader(value ="Authorization", required = true) String authorizationOFSC,
			@NotNull @ApiParam(value = "Id del recurso es requerido", required = true) @NotBlank @Valid @PathVariable(value = "resourceId", required = true) String resourceId,
			@RequestHeader(value = "operationId", required = false) Integer operationId,
			@RequestHeader(value = "transactionId", required = false) Integer transactionId) {

		ResponseEntity<?> responseEntity = null;

		ResourceResponseMessage responseRest = null;
		CanonicalResponseMessage responseInvoke = null;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpStatus httpStatusCode;

		try {

			responseRest = new ResourceResponseMessage();

			responseInvoke = resourceService.invokeGetRestMgwResource(uriMGWServletRest, authorizationOFSC,
					idRequestMGWGetResource, transactionId, resourceId);

			if (operationId != null)
				logger.info("operationId: " + operationId);

			responseRest.setMessage(responseInvoke.getMessage());

			if ("200".equals(responseInvoke.getCode())) {
				httpHeaders.set("code", responseInvoke.getCode());
				httpHeaders.set("message", "Consulta terminada");
				httpHeaders.setContentType(MediaType.APPLICATION_JSON);

				httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseInvoke.getCode()));

				responseRest.setCode(responseInvoke.getCode());
				responseRest.setMessage("Consulta terminada");
				responseRest.setResponse(responseInvoke.getResponse());
			} else {
				if ("999".equals(responseInvoke.getCode()) || "998".equals(responseInvoke.getCode())) {
					httpHeaders.set("message", "Error en servicio interno: : " + responseInvoke.getMessage());
				} else {
					httpHeaders.set("message", "Error en servicio externo: : " + responseInvoke.getMessage());
				}

				httpHeaders.set("code", responseInvoke.getCodeCatch());
				responseRest.setCode(responseInvoke.getCodeCatch());

				httpHeaders.setContentType(MediaType.APPLICATION_JSON);

				httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseInvoke.getCodeCatch()));
			}

			logger.info("responseGetResource: " + responseRest.toString());

			responseEntity = new ResponseEntity<>(responseRest, httpHeaders, httpStatusCode);

		} catch (Exception e) {
			httpHeaders.set("code", String.valueOf(HttpStatus.BAD_REQUEST.value()));
			httpHeaders.set("message", "Error Microservicio: " + e.getMessage());

			responseEntity = new ResponseEntity<>(e.getMessage(), httpHeaders, HttpStatus.valueOf(400));
		}

		return responseEntity;
	}

	public ResponseEntity<?> createResource(
//			@ApiParam(value = "", required = true) @RequestHeader(value ="Authorization", required = true) String authorizationOFSC,

			@NotNull @ApiParam(value = "Id del recurso es requerido", required = true) @NotBlank @Valid @PathVariable(value = "resourceId", required = true) String resourceId,
			@RequestHeader(value = "operationId", required = false) Integer operationId,
			@RequestHeader(value = "transactionId", required = false) Integer transactionId,
			@Valid @RequestBody RequestResource requestResource) {

		ResponseEntity<?> responseEntity = null;

		ResourceResponseMessage responseRest = null;
		CanonicalResponseMessage responseInvoke = null;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpStatus httpStatusCode;

		try {

			responseRest = new ResourceResponseMessage();

			if (requestResource != null) {
				// invocar Rest resource
				responseInvoke = resourceService.invokeCreateRestMgwResource(requestResource, uriMGWServletRest,
						authorizationOFSC, idRequestMGWCreateResource, transactionId, resourceId);

				if (operationId != null)
					logger.info("operationId: " + operationId);

				responseRest.setMessage(responseInvoke.getMessage());

				if ("201".equals(responseInvoke.getCode())) {
					httpHeaders.set("code", responseInvoke.getCode());
					httpHeaders.set("message", "Recurso creado");
					httpHeaders.setContentType(MediaType.APPLICATION_JSON);

					httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseInvoke.getCode()));

					responseRest.setCode(responseInvoke.getCode());
					responseRest.setMessage("Recurso creado");
					responseRest.setResponse(responseInvoke.getResponse());

				} else {
					if ("999".equals(responseInvoke.getCode()) || "998".equals(responseInvoke.getCode())) {
						httpHeaders.set("message", "Error en servicio interno: : " + responseInvoke.getMessage());
					} else {
						httpHeaders.set("message", "Error en servicio externo: : " + responseInvoke.getMessage());
					}

					httpHeaders.set("code", responseInvoke.getCodeCatch());
					responseRest.setCode(responseInvoke.getCodeCatch());

					httpHeaders.setContentType(MediaType.APPLICATION_JSON);

					httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseInvoke.getCodeCatch()));
				}
			} else {
				responseRest.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				responseRest.setMessage("El request del recurso es nulo");

				httpHeaders.set("code", responseRest.getCode());
				httpHeaders.set("message", responseRest.getMessage());
				httpHeaders.setContentType(MediaType.APPLICATION_JSON);

				httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseRest.getCode()));
			}

			logger.info("responseCreateResource: " + responseRest.toString());

			responseEntity = new ResponseEntity<>(responseRest, httpHeaders, httpStatusCode);

		} catch (Exception e) {
			e.printStackTrace();
			httpHeaders.set("code", String.valueOf(HttpStatus.BAD_REQUEST.value()));
			httpHeaders.set("message", "Error Microservicio: " + e.getMessage());

			responseEntity = new ResponseEntity<>(e.getMessage(), httpHeaders,
					HttpStatus.valueOf(HttpStatus.BAD_REQUEST.value()));
		}

		return responseEntity;
	}

	public ResponseEntity<?> updateResource(
//			@ApiParam(value = "", required = true) @RequestHeader(value ="Authorization", required = true) String authorizationOFSC,
			@NotNull @ApiParam(value = "Id del recurso es requerido", required = true) @NotBlank @Valid @PathVariable(value = "resourceId", required = true) String resourceId,
			@RequestHeader(value = "operationId", required = false) Integer operationId,
			@RequestHeader(value = "transactionId", required = false) Integer transactionId,
			@Valid @RequestBody RequestResource requestResource) {

		ResponseEntity<?> responseEntity = null;

		ResourceResponseMessage responseRest = null;
		CanonicalResponseMessage responseInvoke = null;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpStatus httpStatusCode;

		try {

			responseRest = new ResourceResponseMessage();

			if (requestResource != null) {
				// invocar Rest Resource
				responseInvoke = resourceService.invokeUpdateRestMgwResource(requestResource, uriMGWServletRest,
						authorizationOFSC, idRequestMGWUpdateResource, transactionId, resourceId);

				if (operationId != null)
					logger.info("operationId: " + operationId);

				responseRest.setMessage(responseInvoke.getMessage());

				if ("200".equals(responseInvoke.getCode())) {
					httpHeaders.set("code", responseInvoke.getCode());
					httpHeaders.set("message", "Recurso actualizado");
					httpHeaders.setContentType(MediaType.APPLICATION_JSON);

					httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseInvoke.getCode()));

					responseRest.setCode(responseInvoke.getCode());
					responseRest.setMessage("Recurso actualizado");
					responseRest.setResponse(responseInvoke.getResponse());

				} else {
					if ("999".equals(responseInvoke.getCode()) || "998".equals(responseInvoke.getCode())) {
						httpHeaders.set("message", "Error en servicio interno: : " + responseInvoke.getMessage());
					} else {
						httpHeaders.set("message", "Error en servicio externo: : " + responseInvoke.getMessage());
					}

					httpHeaders.set("code", responseInvoke.getCodeCatch());
					responseRest.setCode(responseInvoke.getCodeCatch());

					httpHeaders.setContentType(MediaType.APPLICATION_JSON);

					httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseInvoke.getCodeCatch()));
				}
			} else {
				responseRest.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				responseRest.setMessage("El request del recurso es nulo");

				httpHeaders.set("code", responseRest.getCode());
				httpHeaders.set("message", responseRest.getMessage());
				httpHeaders.setContentType(MediaType.APPLICATION_JSON);

				httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseRest.getCode()));
			}

			logger.info("responseUpdateResource: " + responseRest.toString());

			responseEntity = new ResponseEntity<>(responseRest, httpHeaders, httpStatusCode);

		} catch (Exception e) {
			httpHeaders.set("code", String.valueOf(HttpStatus.BAD_REQUEST.value()));
			httpHeaders.set("message", "Error Microservicio: " + e.getMessage());

			responseEntity = new ResponseEntity<>(e.getMessage(), httpHeaders, HttpStatus.valueOf(400));
		}

		return responseEntity;
	}

	public ResponseEntity<?> deleteResource(
//			@ApiParam(value = "", required = true) @RequestHeader(value ="Authorization", required = true) String authorizationOFSC,
			@NotNull @ApiParam(value = "Id del recurso es requerido", required = true) @NotBlank @Valid @PathVariable(value = "resourceId", required = true) String resourceId,
			@RequestHeader(value = "operationId", required = false) Integer operationId,
			@RequestHeader(value = "transactionId", required = false) Integer transactionId) {

		ResponseEntity<?> responseEntity = null;

		ResourceDeleteResponseMessage responseRest = null;
		CanonicalDeleteResponseMessage responseInvoke = null;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpStatus httpStatusCode;

		try {

			responseRest = new ResourceDeleteResponseMessage();

			if (resourceId != null) {
				// invocar Rest Resource
				responseInvoke = resourceService.invokeDeleteRestMgwResource(uriMGWServletRest, authorizationOFSC,
						idRequestMGWDeleteResource, transactionId, resourceId);

				if (operationId != null)
					logger.info("operationId: " + operationId);
				
				responseRest.setMessage(responseInvoke.getMessage());

				if ("200".equals(responseInvoke.getCode())) {
					httpHeaders.set("code", responseInvoke.getCode());
					httpHeaders.set("message", "Recurso eliminado");
					httpHeaders.setContentType(MediaType.APPLICATION_JSON);

					httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseInvoke.getCode()));

					responseRest.setCode(responseInvoke.getCode());
					responseRest.setMessage("Recurso eliminado");

				} else {
					if ("999".equals(responseInvoke.getCode()) || "998".equals(responseInvoke.getCode())) {
						httpHeaders.set("message", "Error en servicio interno: : " + responseInvoke.getMessage());
					} else {
						httpHeaders.set("message", "Error en servicio externo: : " + responseInvoke.getMessage());
					}

					httpHeaders.set("code", responseInvoke.getCodeCatch());
					responseRest.setCode(responseInvoke.getCodeCatch());

					httpHeaders.setContentType(MediaType.APPLICATION_JSON);

					httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseInvoke.getCodeCatch()));
				}
			} else {
				responseRest.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				responseRest.setMessage("El request del recurso es nulo");

				httpHeaders.set("code", responseRest.getCode());
				httpHeaders.set("message", responseRest.getMessage());
				httpHeaders.setContentType(MediaType.APPLICATION_JSON);

				httpStatusCode = HttpStatus.valueOf(Integer.valueOf(responseRest.getCode()));
			}

			logger.info("responseDeleteActivity: " + responseRest.toString());

			responseEntity = new ResponseEntity<>(responseRest, httpHeaders, httpStatusCode);

		} catch (Exception e) {
			httpHeaders.set("code", String.valueOf(HttpStatus.BAD_REQUEST.value()));
			httpHeaders.set("message", "Error Microservicio: " + e.getMessage());

			responseEntity = new ResponseEntity<>(e.getMessage(), httpHeaders, HttpStatus.valueOf(400));
		}

		return responseEntity;
	}

}
