package com.claro.resources.bean.resources;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactMedium implements Serializable {

	private static final long serialVersionUID = 4311086713683913122L;

	private boolean preferred;
	private String type;
	private String value;

}
