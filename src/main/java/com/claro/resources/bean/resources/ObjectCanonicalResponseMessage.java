package com.claro.resources.bean.resources;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ObjectCanonicalResponseMessage implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8405256173560453379L;
	private CanonicalResponseMessage resourceResponseMessage;
	
}
