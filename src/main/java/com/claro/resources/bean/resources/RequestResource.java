package com.claro.resources.bean.resources;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.claro.resources.enums.ResourceType;
import com.claro.resources.enums.StatusType;
import com.claro.resources.enums.VehicleType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestResource implements Serializable {

	private static final long serialVersionUID = 3690797609546408837L;

	@NotNull(message = "Nombre del recurso es requerido")
	private String name;

	@NotNull(message = "Identificacion unica del recurso padre es requerido")
	private String parentId;

	@NotNull(message = "Tipo de recurso es requerido")
	private ResourceType type;

	@NotNull(message = "Estado del recurso es requerido")
	private StatusType status;

	private String technicianNameFirst;
	private String technicianNameSecond;
	private String technicianNameThird;

	@NotNull(message = "Tipo de vehiculo es requerido")
	private VehicleType vehicleType;

	@NotNull(message = "Contratista es requerido")
	private String contractor;

	private List<ContactMedium> contactMedium;

}
