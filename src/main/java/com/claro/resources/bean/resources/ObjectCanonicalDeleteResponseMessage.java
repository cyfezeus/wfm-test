package com.claro.resources.bean.resources;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ObjectCanonicalDeleteResponseMessage implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4988116449483813293L;
	/**
	 * 
	 */
	private CanonicalDeleteResponseMessage deleteResourceResponseMessage;
	
}
