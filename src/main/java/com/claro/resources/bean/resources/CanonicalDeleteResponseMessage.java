package com.claro.resources.bean.resources;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CanonicalDeleteResponseMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1331791546152486547L;
	private String code;
	private String message;

	@JsonInclude(Include.NON_NULL)
	private String codeCatch;
	
}
