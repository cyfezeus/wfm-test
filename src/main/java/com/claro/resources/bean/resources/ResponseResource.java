package com.claro.resources.bean.resources;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseResource implements Serializable {

	private static final long serialVersionUID = -7132467436400705451L;
	private String resourceId;

	@JsonInclude(Include.NON_NULL)
	private String name;

	@JsonInclude(Include.NON_NULL)
	private String parentId;

	@JsonInclude(Include.NON_NULL)
	private String type;

	@JsonInclude(Include.NON_NULL)
	private String status;

	@JsonInclude(Include.NON_NULL)
	private String technicianNameFirst;

	@JsonInclude(Include.NON_NULL)
	private String technicianNameSecond;

	@JsonInclude(Include.NON_NULL)
	private String technicianNameThird;

	@JsonInclude(Include.NON_NULL)
	private String vehicleType;

	@JsonInclude(Include.NON_NULL)
	private String contractor;

	@JsonInclude(Include.NON_NULL)
	private List<ContactMedium> contactMedium;

}
