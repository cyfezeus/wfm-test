package com.claro.resources.bean.resources;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-24T22:39:10.557Z")
public class ResourceResponseMessage {

	@ApiModelProperty(value = "Service response code")

	@JsonProperty("code")
	private String code;

	@ApiModelProperty(value = "Service response message")

	@JsonProperty("message")
	private String message;

	@JsonProperty("response")
	@JsonInclude(Include.NON_EMPTY)
	private ResourceInfo response;

}
