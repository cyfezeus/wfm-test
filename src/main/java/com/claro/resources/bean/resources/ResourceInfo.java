package com.claro.resources.bean.resources;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResourceInfo implements Serializable {

	private static final long serialVersionUID = -579790605923599827L;

	private ResponseResource resource;
}
